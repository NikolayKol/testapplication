package com.example.myapplication.domain.usecases

import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.domain.repositories.WeatherRepository
import io.reactivex.Single

class WeatherUseCases(private val repo: WeatherRepository) {

	fun loadInfo(lat: Float, lon: Float): Single<WeatherInfo> = repo.getWeatherInfo(lat, lon)

}