package com.example.myapplication.domain.models

data class WeatherInfo(val main: MainInfo, val weather: Weather, val wind: Wind)

data class Weather(val main: String, val description: String)
data class Wind(val speed: Float, val deg: Float)
data class MainInfo(val temp: Float, val pressure: Int)
