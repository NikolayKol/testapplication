package com.example.myapplication.presentation.map

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.R
import com.example.myapplication.presentation.base.mvp.BaseMVPActivity
import com.example.myapplication.presentation.weather.WeatherActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.layout_map_controls.*

class MapActivity : BaseMVPActivity<MapView, MapPresenter>(), MapView, OnMapReadyCallback {

	override val presenter: MapPresenter
		get() = presentersComponent.mapPresenter()

	private var map: GoogleMap? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		setContentView(R.layout.activity_map)
		super.onCreate(savedInstanceState)
		val mapFragment = SupportMapFragment()
		supportFragmentManager
			.beginTransaction()
			.add(R.id.mapContainer, mapFragment)
			.commit()

		mapFragment.getMapAsync(this)

		myLocationBtn.setOnClickListener { presenter.myLocationClicked() }
		zoomInBtn.setOnClickListener { presenter.zoomInClicked() }
		zoomOutBtn.setOnClickListener { presenter.zoomOutClicked() }
		weatherHereBtn.setOnClickListener { presenter.weatherHereClicked() }
	}

	override fun onRequestPermissionsResult(
		requestCode: Int,
		permissions: Array<out String>,
		grantResults: IntArray
	) {
		if (requestCode == PERMISSION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			presenter.locationPermissionGranted()
		}
	}

	//region view methods
	override fun showLoading(show: Boolean) {
		progress.visibility = if (show) View.VISIBLE else View.GONE
	}

	override fun showControls(show: Boolean) {
		val visibility = if (show) View.VISIBLE else View.GONE

		mapControls.visibility = visibility
		weatherHereBtn.visibility = visibility
		marker.visibility = visibility
	}

	override fun checkLocationPermission(): Boolean {
		return ContextCompat.checkSelfPermission(
			this,
			Manifest.permission.ACCESS_FINE_LOCATION
		) == PackageManager.PERMISSION_GRANTED
	}

	override fun requestLocationPermission() {
		ActivityCompat.requestPermissions(
			this,
			arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
			PERMISSION_REQUEST_CODE
		)
	}

	override fun moveCamera(cameraPosition: CameraPosition) {
		map?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
	}

	override fun navigateToShowWeather(coordinates: LatLng) {
		WeatherActivity.startActivity(
			this,
			coordinates.latitude.toFloat(),
			coordinates.longitude.toFloat()
		)
	}

	override fun showLocationLoading(show: Boolean) {
		locationProgress.visibility = if (show) View.VISIBLE else View.INVISIBLE
		myLocationBtn.visibility = if (show) View.INVISIBLE else View.VISIBLE
	}
	//endregion

	override fun onMapReady(googleMap: GoogleMap) {
		map = googleMap
		presenter.onMapReady()
		map?.apply {
			setOnCameraIdleListener { presenter.cameraPosition = cameraPosition }
		}
	}


	companion object {
		private const val PERMISSION_REQUEST_CODE = 433
	}
}
