package com.example.myapplication.presentation.base.mvp

open class BaseMVPPresenter<V : BaseMVPView> {

	protected var view: V? = null

	fun attach(view: V) {
		this.view = view
	}

	fun detach() {
		this.view = null
	}

	open fun onCreate() {}

	open fun onStart() {}

	open fun onResume() {}

	open fun onPause() {}

	open fun onStop() {}

	open fun onDestroy() {}
}