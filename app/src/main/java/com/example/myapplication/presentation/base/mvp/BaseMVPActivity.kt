package com.example.myapplication.presentation.base.mvp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.presentation.base.app.WeatherApp
import com.example.myapplication.presentation.base.di.components.PresentersComponent

@Suppress("UNCHECKED_CAST")
abstract class BaseMVPActivity<V : BaseMVPView, P : BaseMVPPresenter<V>> : AppCompatActivity() {

	protected abstract val presenter: P

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		presenter.attach(this as V)
		presenter.onCreate()
	}

	override fun onStart() {
		super.onStart()
		presenter.onStart()
	}

	override fun onResume() {
		super.onResume()
		presenter.onResume()
	}

	override fun onPause() {
		super.onPause()
		presenter.onPause()
	}

	override fun onStop() {
		super.onStop()
		presenter.onStop()
	}

	override fun onDestroy() {
		super.onDestroy()
		presenter.onDestroy()
		presenter.detach()
	}

	val presentersComponent: PresentersComponent
		get() = (applicationContext as WeatherApp).presentersComponent
}