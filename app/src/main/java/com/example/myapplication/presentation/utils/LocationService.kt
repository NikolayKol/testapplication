package com.example.myapplication.presentation.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single

class LocationService(private val context: Context) {

	@SuppressLint("MissingPermission")
	fun requestLocation(): Single<LatLng> {
		return Single.create<LatLng> {

			val criteria = Criteria().apply {
				accuracy = Criteria.ACCURACY_COARSE
			}

			val locationListener = object : LocationListener {
				override fun onLocationChanged(location: Location) {
					it.onSuccess(LatLng(location.latitude, location.longitude))
				}

				override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
				}

				override fun onProviderEnabled(provider: String?) {
				}

				override fun onProviderDisabled(provider: String?) {
				}
			}

			(context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)
				.requestSingleUpdate(criteria, locationListener, null)

		}
	}
}