package com.example.myapplication.presentation.base.di.modules

import android.content.Context
import com.example.myapplication.R
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@Module
class RetrofitModule(private val context: Context) {

	@Provides
	fun retrofit(apiKey: String): Retrofit {
		val baseUrl = context.getString(R.string.base_url)

		val httpClient = OkHttpClient
			.Builder()
			.addInterceptor { chain ->
				val original = chain.request()
				val originalHttpUrl = original.url()

				val url = originalHttpUrl.newBuilder()
					.addQueryParameter("APPID", apiKey)
					.build()

				// Request customization: add request headers
				val requestBuilder = original.newBuilder()
					.url(url)

				val request = requestBuilder.build()
				chain.proceed(request)
			}
			.build()


		return Retrofit
			.Builder()
			.baseUrl(baseUrl)
			.client(httpClient)
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.build()
	}

}