package com.example.myapplication.presentation.base.di.components

import com.example.myapplication.domain.usecases.WeatherUseCases
import com.example.myapplication.presentation.base.di.modules.ApiModule
import com.example.myapplication.presentation.base.di.modules.RepositoriesModule
import com.example.myapplication.presentation.base.di.modules.UseCasesModule
import com.example.myapplication.presentation.base.di.modules.UtilsModule
import com.example.myapplication.presentation.utils.LocationService
import dagger.Component

@Component(
	dependencies = [AppComponent::class],
	modules = [ApiModule::class, RepositoriesModule::class, UseCasesModule::class, UtilsModule::class]
)
interface UseCaseComponent {
	fun locationService(): LocationService

	fun weatherUseCases(): WeatherUseCases
}