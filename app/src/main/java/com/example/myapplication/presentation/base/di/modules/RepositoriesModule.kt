package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.data.network.WeatherApi
import com.example.myapplication.data.repositories.WeatherRepositoryImpl
import com.example.myapplication.domain.repositories.WeatherRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoriesModule {
	@Provides
	fun weatherRepository(api: WeatherApi): WeatherRepository = WeatherRepositoryImpl(api)
}