package com.example.myapplication.presentation.map

import com.example.myapplication.presentation.base.mvp.BaseMVPView
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng

interface MapView : BaseMVPView {
	fun showLoading(show: Boolean)

	fun showLocationLoading(show: Boolean)

	fun showControls(show: Boolean)

	fun checkLocationPermission(): Boolean

	fun requestLocationPermission()

	fun moveCamera(cameraPosition: CameraPosition)

	fun navigateToShowWeather(coordinates: LatLng)
}