package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.domain.usecases.WeatherUseCases
import com.example.myapplication.presentation.base.di.PresentersScope
import com.example.myapplication.presentation.map.MapPresenter
import com.example.myapplication.presentation.utils.LocationService
import com.example.myapplication.presentation.weather.WeatherPresenter
import dagger.Module
import dagger.Provides

@Module
class PresentersModule {

	@Provides
	@PresentersScope
	fun mapPresenter(locationService: LocationService): MapPresenter = MapPresenter(locationService)

	@Provides
	@PresentersScope
	fun weatherPresenter(weatherUseCases: WeatherUseCases): WeatherPresenter {
		return WeatherPresenter(weatherUseCases)
	}
}