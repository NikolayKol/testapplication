package com.example.myapplication.presentation.base.di.components

import android.content.Context
import com.example.myapplication.presentation.base.di.modules.AppDataModule
import com.example.myapplication.presentation.base.di.modules.RetrofitModule
import dagger.Component
import retrofit2.Retrofit

@Component(modules = [RetrofitModule::class, AppDataModule::class])
interface AppComponent {
	fun retrofit(): Retrofit

	fun context(): Context
}