package com.example.myapplication.presentation.weather

import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.domain.usecases.WeatherUseCases
import com.example.myapplication.presentation.base.mvp.BaseMVPPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException

class WeatherPresenter(
	private val weatherUseCases: WeatherUseCases
) : BaseMVPPresenter<WeatherView>() {

	private var weatherInfo: WeatherInfo? = null

	private var lat: Float = 0f
	private var lon: Float = 0f

	private var disposable: Disposable? = null

	fun setCoordinates(lat: Float, lon: Float) {
		if (this.lat == lat && this.lon == lon && weatherInfo != null) {
			view?.showWeather(weatherInfo!!)
		} else {
			this.lat = lat
			this.lon = lon
			loadWeather()
		}
	}

	private fun loadWeather() {
		view?.showLoading(true)
		disposable?.dispose()
		disposable = weatherUseCases
			.loadInfo(lat, lon)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(
				{
					weatherInfo = it
					view?.showLoading(false)
					view?.showWeather(it)
				},
				{
					view?.showLoading(false)
					if (it is IOException) {
						view?.showConnectionError(::loadWeather)
					} else {
						view?.showError()
					}
				}
			)
	}
}