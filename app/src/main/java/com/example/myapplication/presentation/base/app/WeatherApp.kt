package com.example.myapplication.presentation.base.app

import android.app.Application
import com.example.myapplication.presentation.base.di.components.*
import com.example.myapplication.presentation.base.di.modules.*

class WeatherApp : Application() {

	private val appComponent: AppComponent by lazy {
		DaggerAppComponent
			.builder()
			.appDataModule(AppDataModule(this))
			.retrofitModule(RetrofitModule(this))
			.build()
	}

	private val useCasesComponent: UseCaseComponent by lazy {
		DaggerUseCaseComponent
			.builder()
			.appComponent(appComponent)
			.apiModule(ApiModule())
			.utilsModule(UtilsModule())
			.repositoriesModule(RepositoriesModule())
			.useCasesModule(UseCasesModule())
			.build()
	}

	val presentersComponent: PresentersComponent by lazy {
		DaggerPresentersComponent
			.builder()
			.useCaseComponent(useCasesComponent)
			.presentersModule(PresentersModule())
			.build()
	}
}