package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.domain.repositories.WeatherRepository
import com.example.myapplication.domain.usecases.WeatherUseCases
import dagger.Module
import dagger.Provides

@Module
class UseCasesModule {
	@Provides
	fun weatherUseCases(repo: WeatherRepository): WeatherUseCases = WeatherUseCases(repo)
}