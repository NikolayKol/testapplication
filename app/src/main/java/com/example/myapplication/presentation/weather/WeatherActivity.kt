package com.example.myapplication.presentation.weather

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.presentation.base.mvp.BaseMVPActivity
import kotlinx.android.synthetic.main.activity_weather.*

class WeatherActivity : BaseMVPActivity<WeatherView, WeatherPresenter>(), WeatherView {

	override val presenter: WeatherPresenter
		get() = presentersComponent.weatherPresenter()


	override fun onCreate(savedInstanceState: Bundle?) {
		setContentView(R.layout.activity_weather)
		super.onCreate(savedInstanceState)
		val lat = intent.getFloatExtra(LAT_KEY, 0f)
		val lon = intent.getFloatExtra(LON_KEY, 0f)
		presenter.setCoordinates(lat, lon)
	}

	//region view methods
	override fun showLoading(show: Boolean) {
		progress.visibility = if (show) View.VISIBLE else View.GONE
		dataFields.visibility = if (show) View.INVISIBLE else View.VISIBLE
	}

	override fun showConnectionError(retry: () -> Unit) {
		AlertDialog
			.Builder(this)
			.setMessage(R.string.no_connection)
			.setPositiveButton(R.string.retry) { _, _ -> retry.invoke() }
			.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss(); finish() }
			.show()
	}

	override fun showError() {
		Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show()
		finish()
	}

	override fun showWeather(weatherInfo: WeatherInfo) {
		mainTV.text = weatherInfo.weather.main
		descriptionTV.text = weatherInfo.weather.description

		//TODO
		weatherInfoTV.text = weatherInfo.main.pressure.toString() + " " + weatherInfo.main.temp
		windInfoTV.text = weatherInfo.wind.speed.toString() + " " + weatherInfo.wind.deg.toString()
	}

	//endregion

	companion object {
		private const val LAT_KEY = "lat"
		private const val LON_KEY = "lon"

		fun startActivity(context: Context, lat: Float, lon: Float) {
			val intent = Intent(context, WeatherActivity::class.java).apply {
				putExtra(LAT_KEY, lat)
				putExtra(LON_KEY, lon)
			}
			context.startActivity(intent)
		}
	}
}