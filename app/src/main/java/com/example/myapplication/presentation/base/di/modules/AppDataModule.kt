package com.example.myapplication.presentation.base.di.modules

import android.content.Context
import com.example.myapplication.R
import dagger.Module
import dagger.Provides

@Module
class AppDataModule(private val context: Context) {
	@Provides
	fun provideApiKey(): String {
		return context.getString(R.string.apiKey)
	}

	@Provides
	fun context(): Context = context
}