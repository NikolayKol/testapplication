package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.data.network.WeatherApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {

	@Provides
	fun weatherApi(retrofit: Retrofit): WeatherApi = retrofit.create(WeatherApi::class.java)
}