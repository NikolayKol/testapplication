package com.example.myapplication.presentation.map

import android.annotation.SuppressLint
import com.example.myapplication.presentation.base.mvp.BaseMVPPresenter
import com.example.myapplication.presentation.utils.LocationService
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng

class MapPresenter(
	private val locationService: LocationService
) : BaseMVPPresenter<MapView>() {

	var cameraPosition: CameraPosition
		get() = state.cameraPosition
		set(value) {
			state.cameraPosition = value
		}

	private var state = State()

	override fun onCreate() {
		state.loading = true
		state.controlsVisible = false
		updateViewState()
	}

	fun onMapReady() {
		state.loading = false
		state.controlsVisible = true
		updateViewState()
	}

	fun myLocationClicked() {
		if (view!!.checkLocationPermission()) {
			locationPermissionGranted()
		} else {
			view!!.requestLocationPermission()
		}
	}

	@SuppressLint("CheckResult")
	fun locationPermissionGranted() {
		state.locationLoading = true
		updateViewState()
		locationService
			.requestLocation()
			.subscribe(
				{
					locationReceived(it)
				},
				{
					state.locationLoading = false
					updateViewState()
				}
			)
	}

	fun zoomInClicked() {
		state.cameraPosition = CameraPosition(
			state.cameraPosition.target,
			state.cameraPosition.zoom + ZOOM_STEP,
			state.cameraPosition.tilt,
			state.cameraPosition.bearing
		)
		updateViewState()
	}

	fun zoomOutClicked() {
		state.cameraPosition = CameraPosition(
			state.cameraPosition.target,
			state.cameraPosition.zoom - ZOOM_STEP,
			state.cameraPosition.tilt,
			state.cameraPosition.bearing
		)
		updateViewState()
	}

	fun weatherHereClicked() {
		state.cameraPosition.let { view?.navigateToShowWeather(it.target) }
	}

	private fun locationReceived(latLng: LatLng) {
		state.locationLoading = false
		updateViewState()


		state.cameraPosition = CameraPosition(
			latLng,
			state.cameraPosition.zoom,
			state.cameraPosition.tilt,
			state.cameraPosition.bearing
		)

		view?.moveCamera(state.cameraPosition)
	}

	private fun updateViewState() {
		view?.showLoading(state.loading)
		view?.showControls(state.controlsVisible)
		view?.showLocationLoading(state.locationLoading)
		view?.moveCamera(state.cameraPosition)
	}


	class State {
		var loading = true
		var locationLoading = false
		var controlsVisible = false
		var cameraPosition = CameraPosition(LatLng(55.751244, 37.618423), 10f, 0f, 0f)
	}

	companion object {
		private const val ZOOM_STEP = 1f
	}
}