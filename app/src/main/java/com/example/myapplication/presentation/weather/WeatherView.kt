package com.example.myapplication.presentation.weather

import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.presentation.base.mvp.BaseMVPView

interface WeatherView : BaseMVPView {

	fun showLoading(show: Boolean)

	fun showConnectionError(retry: () -> Unit)

	fun showError()

	fun showWeather(weatherInfo: WeatherInfo)
}