package com.example.myapplication.presentation.base.di.modules

import android.content.Context
import com.example.myapplication.presentation.utils.LocationService
import dagger.Module
import dagger.Provides


@Module
class UtilsModule {
	@Provides
	fun locationService(context: Context): LocationService {
		return LocationService(context)
	}
}