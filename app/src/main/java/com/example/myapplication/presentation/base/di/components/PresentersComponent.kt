package com.example.myapplication.presentation.base.di.components

import com.example.myapplication.presentation.base.di.PresentersScope
import com.example.myapplication.presentation.base.di.modules.PresentersModule
import com.example.myapplication.presentation.map.MapPresenter
import com.example.myapplication.presentation.weather.WeatherPresenter
import dagger.Component

@PresentersScope
@Component(
	dependencies = [UseCaseComponent::class],
	modules = [PresentersModule::class]
)
interface PresentersComponent {
	fun mapPresenter(): MapPresenter

	fun weatherPresenter(): WeatherPresenter
}