package com.example.myapplication.data.models

import com.google.gson.annotations.SerializedName

data class WeatherInfoTO(
	@SerializedName("weather") val weather: WeatherTO,
	@SerializedName("wind") val wind: WindTO,
	@SerializedName("main") val main: MainInfoTO
)

data class WindTO(
	@SerializedName("speed") val speed: Float,
	@SerializedName("deg") val deg: Float
)

data class WeatherTO(
	@SerializedName("main") val main: String,
	@SerializedName("description") val description: String
)

data class MainInfoTO(
	@SerializedName("temp") val temp: Float,
	@SerializedName("pressure") val pressure: Int
)