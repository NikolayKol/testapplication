package com.example.myapplication.data.repositories

import com.example.myapplication.data.network.WeatherApi
import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.domain.repositories.WeatherRepository
import io.reactivex.Single
import java.io.IOException

class WeatherRepositoryImpl(private val api: WeatherApi) : WeatherRepository {
	override fun getWeatherInfo(lat: Float, lon: Float): Single<WeatherInfo> {
//		return api.getWeatherInfo(lat, lon).map { it.toModel() }
		return Single.fromCallable {
			Thread.sleep(3000)
			throw IOException()
		}
	}
}