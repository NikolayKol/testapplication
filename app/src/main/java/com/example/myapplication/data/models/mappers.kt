package com.example.myapplication.data.models

import com.example.myapplication.domain.models.MainInfo
import com.example.myapplication.domain.models.Weather
import com.example.myapplication.domain.models.WeatherInfo
import com.example.myapplication.domain.models.Wind

fun WeatherInfoTO.toModel(): WeatherInfo {
	return WeatherInfo(
		main.toModel(),
		weather.toModel(),
		wind.toModel()
	)
}

fun MainInfoTO.toModel(): MainInfo {
	return MainInfo(temp, pressure)
}

fun WeatherTO.toModel(): Weather {
	return Weather(main, description)
}

fun WindTO.toModel(): Wind {
	return Wind(speed, deg)
}