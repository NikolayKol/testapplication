package com.example.myapplication.data.network

import com.example.myapplication.data.models.WeatherInfoTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

	@GET("/data/2.5/weather")
	fun getWeatherInfo(
		@Query("lat") lat: Float,
		@Query("lon") lon: Float
	): Single<WeatherInfoTO>

}